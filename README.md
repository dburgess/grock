# Grock

![](demo-images/icon.png)

GTK application to display geological maps of the UK, powered by [libchamplain](https://gitlab.gnome.org/GNOME/libchamplain).

Base map tiles provided by [Mapbox](https://www.mapbox.com/), geological data provided by the [British Geological Survey](https://www.bgs.ac.uk)'s various [Web Map Services](https://www.bgs.ac.uk/data/services/wms.html).

Built with GNOME Builder.

** NOTE: this program is not yet stable. Expect crashes every now and then. ** 

## Features

![](demo-images/grock-satellite.png)
Choose between satellite and 'street-style' views

![](demo-images/grock-features.png)
Right-click on a rock type to find out more

![](demo-images/grock-magnetic.png)
![](demo-images/grock-gravity.png)
View small-scale gravitional and magnetic anomaly maps of the UK

![](demo-images/grock-linear.png)
Toggle linear features (e.g. faults)

![](demo-images/grock-toolbar.png)

Explanation of toolbar, since the icons are slightly arbitrary for now (from left-to-right):
* Adjust opacity of geological overlay
* Toggle linear features
* Toggle magnetic anomaly (visible when zoomed out)
* Toggle gravitational anomaly (visible when zoomed out)
* Toggle satellite view
* Toggle search bar (not yet implemented)
* Primary menu

## To do

- [ ] Location search 
- [X] Rock information on right-click
- [ ] Find current location
- [X] Satellite view
- [ ] Keyboard shortcuts / help dialog
- [ ] Find / design better icons for toolbar

## License

Grock is licensed under the GNU GPLv3 Licence - see [COPYING](COPYING).

## Many thanks to

* Maintainers of and contributers to libchamplain
* Contributers to gnome-recipes, gnome-maps, gnome-photos and Glade. Inspection of the source code of these programs was useful in solving problems.
* The many people willing to help out on the #gnome-newbies and #gnome-maps IRC channels.
