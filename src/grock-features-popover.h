/* grock-features-popover.h
 *
 * Copyright © 2019 Daniel Burgess
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GROCK_FEATURES_POPOVER_H
#define GROCK_FEATURES_POPOVER_H

#include <gtk-3.0/gtk/gtk.h>

#include "grock-features.h"

typedef struct
{
  gdouble lat;
  gdouble lon;

  gchar *bng_string;

  gchar *heading;
  gchar *min_specific_time;
  gchar *max_specific_time;
  gchar *min_period;
  gchar *max_period;
  gchar *rock_type;
  gchar *setting;
  gchar *setting_plus;
  gchar *environment;
} GrockFeatureInfo;

typedef struct
{
  GrockFeatureInfo *info;

  GtkWidget *pop;

  GtkWidget *main_grid;

  GtkWidget *stack;
  GtkWidget *spinner;
  GtkWidget *spin_box;
  GtkWidget *data_grid;

  GtkWidget *lbl_empty;
  GtkWidget *lbl_lat;
  GtkWidget *lbl_lon;
  GtkWidget *lbl_heading;
  GtkWidget *lbl_period;
  GtkWidget *lbl_rock_type;
  GtkWidget *lbl_setting;
  GtkWidget *lbl_setting_plus;
  GtkWidget *lbl_environment;

  GtkWidget *lbl_coords;
} GrockFeaturesPopover;

void grock_features_popover_make         (GtkWidget *widget, gdouble x, gdouble y);
void grock_features_popover_display_info (GrockFeaturesPopover *p);


#endif
