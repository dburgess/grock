/* grock-window.c
 *
 * Copyright © 2019 Daniel Burgess
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <champlain-0.12/champlain/champlain.h>
#include <champlain-0.12/champlain-gtk/champlain-gtk.h>
#include <clutter-gtk-1.0/clutter-gtk/clutter-gtk.h>
#include <gtk/gtk.h>

#include "grock-about-dialog.h"
#include "grock-application.h"
#include "grock-config.h"
#include "grock-features.h"
#include "grock-features-popover.h"
#include "grock-globals.h"
#include "grock-location.h"
#include "grock-map.h"
#include "grock-search.h"
#include "grock-search-popover.h"
#include "grock-window.h"

struct _GrockWindow
{
  GtkApplicationWindow  parent_instance;

  /* Template widgets */
  GtkStack          *main_stack;
  GtkGrid           *no_network_view;
  GtkGrid           *parent_grid;

  GtkChamplainEmbed *map_embed;

  GtkRevealer       *search_revealer;
  //GtkPopover        *search_popover;
  GtkSearchEntry    *search_bar;

  GrockSearchPopover *search_popover;

  GtkHeaderBar      *header;
  GtkButton         *zoom_out_button;
  GtkButton         *zoom_in_button;
  GtkButton         *location_button;
  GtkToggleButton   *satellite_button;
  GtkToggleButton   *magnet_button;
  GtkToggleButton   *gravity_button;
  GtkToggleButton   *linear_button;
  GtkToggleButton   *search_button;
  GtkScaleButton    *opacity_button;
  GtkMenuButton     *menu_button;
  GMenuModel        *hamburger_menu;

  GrockMap          *map;

  GtkWidget         *about_dialog;

  GList             *dialogs;
};

G_DEFINE_TYPE (GrockWindow, grock_window, GTK_TYPE_APPLICATION_WINDOW)

static void
on_search_bar_stop_search (GtkSearchEntry *entry, GrockWindow *window)
{
  gtk_toggle_button_set_active (window->search_button, FALSE);
  gtk_toggle_button_toggled (window->search_button);
  window->search_popover = NULL;
}

/*
static void
on_search_bar_search_changed (GtkSearchEntry *entry, GrockWindow *window)
{
  if (window->search_popover == NULL)
      window->search_popover = grock_search_popover_make (entry);

  g_print ("Search changed.\n");

  grock_search_perform_search (entry, window->search_popover);

} */

static void
on_search_bar_activate (GtkSearchEntry *entry, GrockWindow *window)
{
  const gchar *reference = gtk_entry_get_text (GTK_ENTRY (entry));

  if (!g_strcmp0 (reference, ""))
    return;

  GrockPoint point = grock_search_get_lat_lon_from_reference (reference);

  if (point.lat == 0 && point.lon == 0)
    return;

  grock_map_center_on (window->map, point.lat, point.lon);

  GString *ref_str = g_string_new (reference);
  gint length = ref_str->len;

  g_print ("%d\n", length);

  switch (length)
    {
    case 2:
      grock_map_set_zoom_level (window->map, 6);
      break;
    case 4:
      grock_map_set_zoom_level (window->map, 8);
      break;
    case 6:
      grock_map_set_zoom_level (window->map, 10);
      break;
    case 8:
      grock_map_set_zoom_level (window->map, 12);
      break;
    case 10:
      grock_map_set_zoom_level (window->map, 14);
      break;
    case 12:
      grock_map_set_zoom_level (window->map, 16);
      break;
    default:
      grock_map_set_zoom_level (window->map, 6);
    }

  g_string_free (ref_str, TRUE);
}

static void
on_linear_button_toggled (GtkButton *button, GrockWindow *window)
{
  if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (button)) == TRUE)
    grock_map_set_overlay_linear (window->map);
  else
    grock_map_remove_overlay_linear (window->map);
}

static void
on_magnet_button_toggled (GtkButton *button, GrockWindow *window)
{
  if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (button)) == TRUE)
    grock_map_set_overlay_mag (window->map);
  else
    grock_map_remove_overlay_mag (window->map);
}

static void
on_gravity_button_toggled (GtkButton *button, GrockWindow *window)
{
  if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (button)) == TRUE)
    grock_map_set_overlay_grav (window->map);
  else
    grock_map_remove_overlay_grav (window->map);
}

static void
on_satellite_button_toggled (GtkButton *button, GrockWindow *window)
{
  if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (button)) == TRUE)
    grock_map_set_main_source_sat (window->map);
  else
    grock_map_set_main_source_streets (window->map);

  /**
   * Need to re-add the overlays, since Champlain removes them with the
   * main source.
   */
  grock_map_update_overlay_opacity (window->map, gtk_scale_button_get_value (window->opacity_button));
}

static void
on_zoom_in_button_clicked (GtkButton *button, GrockWindow *window)
{
  grock_map_zoom_in (window->map);
}

static void
on_zoom_out_button_clicked (GtkButton *button, GrockWindow *window)
{
  grock_map_zoom_out (window->map);
}

static void
on_location_button_clicked (GtkButton *button, GrockWindow *window)
{
  GrockLocation location = grock_location_get_location ();
  grock_map_go_to (window->map, location.lat, location.lon);
}

static void
on_search_button_toggled (GtkToggleButton *button, GrockWindow *window)
{
  if (gtk_revealer_get_child_revealed (window->search_revealer) == TRUE)
    gtk_revealer_set_reveal_child (window->search_revealer, FALSE);
  else
    gtk_revealer_set_reveal_child (window->search_revealer, TRUE);
}

static void
on_opacity_button_value_changed (GtkScaleButton *scale, GrockWindow *window)
{
  gint opacity = gtk_scale_button_get_value (window->opacity_button);
  grock_map_update_overlay_opacity (window->map, opacity);
}

static void
grock_window_finalize (GObject *window)
{
  GrockWindow *self = GROCK_WINDOW (window);

  g_list_free (self->dialogs);

  g_object_unref (self->map);

  G_OBJECT_CLASS (grock_window_parent_class)->finalize (G_OBJECT (self));
}

static void
grock_window_class_init (GrockWindowClass *class)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (class);
  GObjectClass *object_class = G_OBJECT_CLASS (class);

  gtk_widget_class_set_template_from_resource (widget_class, "/com/gitlab/dburgess/Grock/grock-window.ui");

  gtk_widget_class_bind_template_child (widget_class, GrockWindow, main_stack);
  gtk_widget_class_bind_template_child (widget_class, GrockWindow, no_network_view);
  gtk_widget_class_bind_template_child (widget_class, GrockWindow, parent_grid);
  gtk_widget_class_bind_template_child (widget_class, GrockWindow, search_revealer);
  gtk_widget_class_bind_template_child (widget_class, GrockWindow, search_popover);
  gtk_widget_class_bind_template_child (widget_class, GrockWindow, search_bar);
  gtk_widget_class_bind_template_child (widget_class, GrockWindow, header);
  gtk_widget_class_bind_template_child (widget_class, GrockWindow, zoom_out_button);
  gtk_widget_class_bind_template_child (widget_class, GrockWindow, zoom_in_button);
  gtk_widget_class_bind_template_child (widget_class, GrockWindow, location_button);
  gtk_widget_class_bind_template_child (widget_class, GrockWindow, search_button);
  gtk_widget_class_bind_template_child (widget_class, GrockWindow, opacity_button);
  gtk_widget_class_bind_template_child (widget_class, GrockWindow, satellite_button);
  gtk_widget_class_bind_template_child (widget_class, GrockWindow, magnet_button);
  gtk_widget_class_bind_template_child (widget_class, GrockWindow, gravity_button);
  gtk_widget_class_bind_template_child (widget_class, GrockWindow, linear_button);

  gtk_widget_class_bind_template_callback (widget_class, on_zoom_out_button_clicked);
  gtk_widget_class_bind_template_callback (widget_class, on_zoom_in_button_clicked);
  gtk_widget_class_bind_template_callback (widget_class, on_location_button_clicked);
  gtk_widget_class_bind_template_callback (widget_class, on_opacity_button_value_changed);

  object_class->finalize = grock_window_finalize;
}

static void
grock_window_init (GrockWindow *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));

  GAction *action;

  GApplication *app = g_application_get_default ();

  action = g_action_map_lookup_action (G_ACTION_MAP (app), "about");
  g_simple_action_set_enabled (G_SIMPLE_ACTION (action), TRUE);

  action = g_action_map_lookup_action (G_ACTION_MAP (app), "quit");
  g_simple_action_set_enabled (G_SIMPLE_ACTION (action), TRUE);

  g_signal_connect (self->location_button,  "clicked",        G_CALLBACK (on_location_button_clicked),      self);
  g_signal_connect (self->search_button,    "toggled",        G_CALLBACK (on_search_button_toggled),        self);
  g_signal_connect (self->satellite_button, "toggled",        G_CALLBACK (on_satellite_button_toggled),     self);
  g_signal_connect (self->magnet_button,    "toggled",        G_CALLBACK (on_magnet_button_toggled),        self);
  g_signal_connect (self->gravity_button,   "toggled",        G_CALLBACK (on_gravity_button_toggled),       self);
  g_signal_connect (self->linear_button,    "toggled",        G_CALLBACK (on_linear_button_toggled),        self);
  g_signal_connect (self->opacity_button,   "value-changed",  G_CALLBACK (on_opacity_button_value_changed), self);
//g_signal_connect (self->search_bar,       "search-changed", G_CALLBACK (on_search_bar_search_changed),    self);
  g_signal_connect (self->search_bar,       "stop-search",    G_CALLBACK (on_search_bar_stop_search),       self);
  g_signal_connect (self->search_bar,       "activate",       G_CALLBACK (on_search_bar_activate),          self);

  grock_window_button_disable (self->location_button);

  gtk_widget_set_size_request (self->search_bar, 100, -1);

  self->search_popover = NULL;
}

void
grock_window_button_disable (GtkButton *button)
{
  gtk_widget_set_sensitive (GTK_WIDGET (button), FALSE);
}

void
grock_window_button_enable (GtkButton *button)
{
  gtk_widget_set_sensitive (GTK_WIDGET (button), TRUE);
}

static void
connect_map_signals (GrockWindow *self)
{
  g_signal_connect (self->zoom_in_button,  "clicked", G_CALLBACK (on_zoom_in_button_clicked),  self);
  g_signal_connect (self->zoom_out_button, "clicked", G_CALLBACK (on_zoom_out_button_clicked), self);
}

static gboolean
on_button_release_map_embed (GtkWidget *widget, GdkEvent *event)
{
  g_return_val_if_fail (widget != NULL, FALSE);
  g_return_val_if_fail (GTK_CHAMPLAIN_IS_EMBED (widget), FALSE);
  g_return_val_if_fail (event != NULL, FALSE);

  GdkEventButton *event_button;

  if (event->type == GDK_BUTTON_RELEASE)
    {
      event_button = (GdkEventButton *)event;

      if (event_button->button == GDK_BUTTON_SECONDARY)
        {
          grock_features_popover_make (widget, event_button->x, event_button->y);

          return TRUE;
        }
    }

  return FALSE;
}

void
grock_window_add_map (GrockWindow *self)
{
  self->map = grock_map_new ();

  connect_map_signals (self);

  grock_map_set_window (self->map, self);

  grock_map_set_main_source_streets (self->map);

  grock_map_set_overlay_s (self->map);

  gtk_scale_button_set_value (self->opacity_button, GROCK_INITIAL_OVERLAY_OPACITY);

  grock_map_finish (self->map);

  self->map_embed = grock_map_get_map_embed (self->map);
  g_signal_connect_swapped (self->map_embed, "button_release_event",
                            G_CALLBACK (on_button_release_map_embed),
                            self->map_embed);

  grock_map_add_to_container (self->map, GTK_CONTAINER (self->main_stack));
  gtk_widget_show_all (GTK_WIDGET (self->parent_grid));
  grock_map_show (self->map);
  grock_map_set_visible_stack_child (self->map, self->main_stack);

  gtk_revealer_set_reveal_child (self->search_revealer, FALSE);
}

static void
on_dialog_unmapped (GtkWidget *dialog, GrockWindow *window)
{
  window->dialogs = g_list_remove (window->dialogs, dialog);
}

/*
 * Function taken from GNOME Recipes
 * https://gitlab.gnome.org/GNOME/recipes
 */
static void
grock_window_present_dialog (GrockWindow *self, GtkWindow *dialog)
{
  GtkWindow *parent;

  if (self->dialogs)
    parent = self->dialogs->data;
  else
    parent = GTK_WINDOW (self);

  gtk_window_set_transient_for (dialog, parent);
  gtk_window_set_modal (dialog, TRUE);

  self->dialogs = g_list_prepend (self->dialogs, dialog);
  g_signal_connect (dialog, "unmap", G_CALLBACK (on_dialog_unmapped), self);

  gtk_window_present (dialog);
}

static void
on_about_response (GtkWidget *dialog, int response, GrockWindow *window)
{
  if (response != GTK_RESPONSE_NONE)
    {
      gtk_widget_destroy (dialog);
      window->about_dialog = NULL;
    }
}

void
grock_window_show_about_dialog (GrockWindow *self)
{
  if (self->about_dialog)
    return;

  self->about_dialog = (GtkWidget *)grock_about_dialog_new ();
  g_signal_connect (self->about_dialog, "response", G_CALLBACK (on_about_response), self);
  grock_window_present_dialog (self, GTK_WINDOW (self->about_dialog));
}

GrockWindow *
grock_window_new (GrockApplication *app)
{
  return g_object_new (GROCK_TYPE_WINDOW, "application", app, NULL);
}
