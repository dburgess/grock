#ifndef GROCK_MAP_H
#define GROCK_MAP_H

#include <champlain-0.12/champlain/champlain.h>
#include <champlain-gtk/champlain-gtk.h>
#include <gtk-3.0/gtk/gtk.h>

#include "grock-window.h"

G_BEGIN_DECLS

#define GROCK_TYPE_MAP grock_map_get_type ()

G_DECLARE_FINAL_TYPE (GrockMap, grock_map, GROCK, MAP, GObject)

#define TILE_CACHE_SIZE_DISK 100000000
#define TILE_CACHE_SIZE_MEMY 50

#define CACHE_PATH ".cache/grock"

#define TILE_SIZE                 512

#define STREETS_SOURCE_ID            "mapbox-outdoors"
#define STREETS_SOURCE_NAME          "mapbox-outdoors"
#define STREETS_SOURCE_LICENCE       "© OpenStreetMap © Mapbox"
#define STREETS_SOURCE_LICENCE_URI   "http://www.openstreetmap.org/copyright"
#define STREETS_SOURCE_MIN_ZOOM      7
#define STREETS_SOURCE_MAX_ZOOM      20
#define STREETS_SOURCE_URI           "https://api.mapbox.com/styles/v1/mapbox/outdoors-v11/tiles/#Z#/#X#/#Y#?access_token=pk.eyJ1IjoiZGJ1cmdlc3MiLCJhIjoiY2p6MnptdDdyMDJsdDNpcWpwNmQ5bTlxMCJ9.Q02VF5EA1nFEZOradKRs_w"

#define SAT_SOURCE_ID                "mapbox-satellite"
#define SAT_SOURCE_NAME              "mapbox-satellite"
#define SAT_SOURCE_LICENCE           "© OpenStreetMap © Mapbox"
#define SAT_SOURCE_LICENCE_URI       "http://www.openstreetmap.org/copyright"
#define SAT_SOURCE_MIN_ZOOM          7
#define SAT_SOURCE_MAX_ZOOM          20
#define SAT_SOURCE_URI               "https://api.mapbox.com/styles/v1/mapbox/satellite-streets-v11/tiles/#Z#/#X#/#Y#?access_token=pk.eyJ1IjoiZGJ1cmdlc3MiLCJhIjoiY2p6MnptdDdyMDJsdDNpcWpwNmQ5bTlxMCJ9.Q02VF5EA1nFEZOradKRs_w"

#define GEO_SRC_LARGE_ID             "bgs-largescale"
#define GEO_SRC_LARGE_NAME           "bgs-largescale"
#define GEO_SRC_LARGE_LICENCE        "Contains British Geological Survey materials © UKRI 2019"
#define GEO_SRC_LARGE_LICENCE_URI    "http://www.nationalarchives.gov.uk/doc/open-government-licence/version/3/"
#define GEO_SRC_LARGE_MIN_ZOOM       12
#define GEO_SRC_LARGE_MAX_ZOOM       15
#define GEO_SRC_LARGE_URI            "https://map.bgs.ac.uk/arcgis/services/BGS_Detailed_Geology/MapServer/WMSServer?REQUEST=GetMap&VERSION=1.3.0&LAYERS=BGS.50k.Bedrock&STYLES=default&FORMAT=image/png&CRS=EPSG:3857&WIDTH=512&HEIGHT=512&BBOX=#L#,#B#,#R#,#T#"

#define GEO_SRC_SMALL_ID             "bgs-smallscale"
#define GEO_SRC_SMALL_NAME           "bgs-smallscale"
#define GEO_SRC_SMALL_LICENCE        "Contains British Geological Survey materials © UKRI 2019"
#define GEO_SRC_SMALL_LICENCE_URI    "http://www.nationalarchives.gov.uk/doc/open-government-licence/version/3/"
#define GEO_SRC_SMALL_MIN_ZOOM       7
#define GEO_SRC_SMALL_MAX_ZOOM       11
#define GEO_SRC_SMALL_URI            "http://ogc.bgs.ac.uk/cgi-bin/BGS_Bedrock_and_Superficial_Geology/wms?version=1.3.0&request=GetMap&layers=GBR_BGS_625k_BLS&format=image/png&crs=epsg:3857&bbox=#L#,#B#,#R#,#T#&width=512&height=512&styles="

#define MAG_SRC_ID             "bgs-magnetism"
#define MAG_SRC_NAME           "bgs-magnetism"
#define MAG_SRC_LICENCE        ""
#define MAG_SRC_LICENCE_URI    "http://www.nationalarchives.gov.uk/doc/open-government-licence/version/3/"
#define MAG_SRC_MIN_ZOOM       6
#define MAG_SRC_MAX_ZOOM       11
#define MAG_SRC_URI            "https://map.bgs.ac.uk/arcgis/services/GeoIndex_Onshore/geophysics/MapServer/WmsServer?REQUEST=GetMap&LAYERS=Magnetic.anomalies&VERSION=1.3.0&STYLES=default&FORMAT=image/png&CRS=EPSG:3857&BBOX=#L#,#B#,#R#,#T#&WIDTH=512&HEIGHT=512"

#define GRAV_SRC_ID             "bgs-gravity"
#define GRAV_SRC_NAME           "bgs-gravity"
#define GRAV_SRC_LICENCE        ""
#define GRAV_SRC_LICENCE_URI    "http://www.nationalarchives.gov.uk/doc/open-government-licence/version/3/"
#define GRAV_SRC_MIN_ZOOM       6
#define GRAV_SRC_MAX_ZOOM       11
#define GRAV_SRC_URI            "https://map.bgs.ac.uk/arcgis/services/GeoIndex_Onshore/geophysics/MapServer/WmsServer?REQUEST=GetMap&LAYERS=Gravity.anomalies.colour.shaded&VERSION=1.3.0&STYLES=default&FORMAT=image/png&CRS=EPSG:3857&BBOX=#L#,#B#,#R#,#T#&WIDTH=512&HEIGHT=512"

#define LINEAR_SRC_ID             "bgs-linear"
#define LINEAR_SRC_NAME           "bgs-linear"
#define LINEAR_SRC_LICENCE        ""
#define LINEAR_SRC_LICENCE_URI    "http://www.nationalarchives.gov.uk/doc/open-government-licence/version/3/"
#define LINEAR_SRC_MIN_ZOOM       6
#define LINEAR_SRC_MAX_ZOOM       11
#define LINEAR_SRC_URI            "https://map.bgs.ac.uk/arcgis/services/BGS_Detailed_Geology/MapServer/WMSServer?REQUEST=GetMap&VERSION=1.3.0&LAYERS=BGS.50k.Linear.features&STYLES=default&FORMAT=image/png&CRS=EPSG:3857&WIDTH=512&HEIGHT=512&BBOX=#L#,#B#,#R#,#T#"

#define ERROR_SRC_ID              "error"
#define ERROR_SRC_NAME            "error"
#define ERROR_SRC_LICENCE         "null"
#define ERROR_SRC_LICENCE_URI     "null"


GrockMap          *grock_map_new                         (void);
void               grock_map_set_background_source_chain (GrockMap *self);
void               grock_map_finish                      (GrockMap *self);
gdouble            grock_map_get_lat                     (GrockMap *self);
gdouble            grock_map_get_lon                     (GrockMap *self);
gint               grock_map_get_zoom_level              (GrockMap *self);
gint               grock_map_get_current_zoom_level      (GrockMap *self);
void               grock_map_update_zoom_level           (GrockMap *self);
void               grock_map_zoom_in                     (GrockMap *self);
void               grock_map_zoom_out                    (GrockMap *self);
void               grock_map_go_to                       (GrockMap *self, gdouble lat, gdouble lon);
void               grock_map_center_on                   (GrockMap *self, gdouble lat, gdouble lon);
void               grock_map_set_overlay_s               (GrockMap *self);
void               grock_map_remove_overlay_s            (GrockMap *self);
void               grock_map_set_overlay_l               (GrockMap *self);
void               grock_map_remove_overlay_l            (GrockMap *self);
void               grock_map_update_overlay_opacity      (GrockMap *self, gint opacity);
void               grock_map_set_window                  (GrockMap *self, GrockWindow *window);
void               grock_map_add_to_container            (GrockMap *self, GtkContainer *container);
void               grock_map_show                        (GrockMap *self);
void               grock_map_set_visible_stack_child     (GrockMap *self, GtkStack *stack);
void               grock_map_set_zoom_level              (GrockMap *self, gint zoom_level);
void               grock_map_set_overlay_opacity         (GrockMap *self, gint opacity);
GtkChamplainEmbed *grock_map_get_map_embed               (GrockMap *self);
void               grock_map_set_main_source_streets     (GrockMap *self);
void               grock_map_set_main_source_sat         (GrockMap *self);
void               grock_map_remove_overlays             (GrockMap *self);
void               grock_map_set_overlay_grav            (GrockMap *self);
void               grock_map_remove_overlay_grav         (GrockMap *self);
void               grock_map_set_overlay_mag             (GrockMap *self);
void               grock_map_remove_overlay_mag          (GrockMap *self);
void               grock_map_set_overlay_linear          (GrockMap *self);
void               grock_map_remove_overlay_linear       (GrockMap *self);

void grock_map_print_current_overlays (GrockMap *self);

void               grock_map_on_map_layer_relocated      (ChamplainView *view, GrockMap *map);

G_END_DECLS

#endif
