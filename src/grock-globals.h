/* grock-globals.h
 *
 * Copyright © 2019 Daniel Burgess
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GROCK_GLOBALS_H
#define GROCK_GLOBALS_H

#define APPLICATION_NAME "Grock"
#define APPLICATION_ID "com.gitlab.dburgess.Grock"
#define APPLICATION_VERSION "0.0.1"

#define GROCK_INITIAL_OVERLAY_OPACITY 50
#define GROCK_INITIAL_ZOOM_LEVEL 6

#endif
