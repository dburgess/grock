/* grock-features.h
 *
 * Copyright © 2019 Daniel Burgess
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GROCK_FEATURES_H
#define GROCK_FEATURES_H

#include <champlain-0.12/champlain/champlain.h>
#include <champlain-0.12/champlain-gtk/champlain-gtk.h>

#include "grock-features-popover.h"

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

#define DEG2RAD(a) ((a) / (180 / M_PI) )
#define RAD2DEG(a) ((a) * (180 / M_PI) )
#define EARTH_RADIUS 6378137

#define FEATURES_BASE_URI "https://map.bgs.ac.uk/arcgis/services/BGS_Detailed_Geology/MapServer/WMSServer?version=1.3.0&request=GetFeatureInfo&format=image/png&layers=BGS.50k.Bedrock&query_layers=BGS.50k.Bedrock&info_format=text/xml&i=0&j=512&crs=EPSG:3857&WIDTH=512&HEIGHT=512&styles=default&BBOX="

typedef struct
{
  gint x;
  gint y;
} GrockTile;

typedef struct
{
  gdouble lon;
  gdouble lat;
} GrockPoint;

GrockTile         grock_features_get_tile_point_from_coords    (gdouble lat, gdouble lon, gint zoomlevel);
GrockPoint        grock_features_get_web_mercators_from_coords (gdouble lat, gdouble lon);
void              grock_features_get_from_epsg3857             (gdouble lat, gdouble lon, GrockFeaturesPopover *p);
void              grock_features_get_bng_from_epsg3857         (gdouble northing, gdouble easting, GrockFeaturesPopover *p);
GrockFeatureInfo *grock_feature_info_new                       (void);
#endif
