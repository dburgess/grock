/* grock-map.c
 *
 * Copyright © 2019 Daniel Burgess
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <champlain-0.12/champlain/champlain.h>
#include <champlain-gtk/champlain-gtk.h>
#include <glib/gi18n.h>

#include "grock-features.h"
#include "grock-globals.h"
#include "grock-map.h"
#include "grock-window.h"

struct _GrockMap
{
  GObject parent_instance;

  GtkChamplainEmbed    *map_embed;
  ChamplainView        *view;
  ClutterActor         *scale;
  ChamplainLicense     *licence_actor;
  ChamplainBoundingBox *bbox;

  GtkPopover           *features_popover;

  ChamplainImageRenderer     *renderer;
  ChamplainErrorTileRenderer *error_renderer;

  ChamplainBoundingBox          *world;

  gint                           overlay_opacity;

  ChamplainNetworkTileSource    *streets_src;
  ChamplainNetworkTileSource    *sat_src;
  ChamplainNetworkWmsTileSource *geol_src_s;
  ChamplainNetworkWmsTileSource *geol_src_l;
  ChamplainNetworkWmsTileSource *mag_src;
  ChamplainNetworkWmsTileSource *grav_src;
  ChamplainNetworkWmsTileSource *linear_src;
  ChamplainNullTileSource       *errr_src;

  ChamplainMapSource            *current_main_source;
  ChamplainMapSource            *current_overlay;

  ChamplainFileCache            *streets_src_file_cache;
  ChamplainMemoryCache          *streets_src_memy_cache;
  ChamplainMapSourceChain       *streets_src_chain;

  ChamplainFileCache            *sat_src_file_cache;
  ChamplainMemoryCache          *sat_src_memy_cache;
  ChamplainMapSourceChain       *sat_src_chain;

  ChamplainFileCache            *geol_src_s_file_cache;
  ChamplainMemoryCache          *geol_src_s_memy_cache;
  ChamplainMapSourceChain       *geol_src_s_chain;

  ChamplainFileCache            *geol_src_l_file_cache;
  ChamplainMemoryCache          *geol_src_l_memy_cache;
  ChamplainMapSourceChain       *geol_src_l_chain;

  ChamplainFileCache            *mag_src_file_cache;
  ChamplainMemoryCache          *mag_src_memy_cache;
  ChamplainMapSourceChain       *mag_src_chain;

  ChamplainFileCache            *grav_src_file_cache;
  ChamplainMemoryCache          *grav_src_memy_cache;
  ChamplainMapSourceChain       *grav_src_chain;

  ChamplainFileCache            *linear_src_file_cache;
  ChamplainMemoryCache          *linear_src_memy_cache;
  ChamplainMapSourceChain       *linear_src_chain;

  gboolean geol_l_is_active;
  gboolean geol_s_is_active;
  gboolean mag_is_active;
  gboolean grav_is_active;
  gboolean linear_is_active;

  gint zoom_level;
};

G_DEFINE_TYPE (GrockMap, grock_map, G_TYPE_OBJECT)

static ChamplainNetworkTileSource *
get_streets_source (ChamplainRenderer *renderer)
{
  return champlain_network_tile_source_new_full (STREETS_SOURCE_ID,
    STREETS_SOURCE_NAME,
    STREETS_SOURCE_LICENCE,
    STREETS_SOURCE_LICENCE_URI,
    STREETS_SOURCE_MIN_ZOOM,
    STREETS_SOURCE_MAX_ZOOM,
    TILE_SIZE,
    CHAMPLAIN_MAP_PROJECTION_MERCATOR,
    STREETS_SOURCE_URI,
    renderer);
}

static ChamplainNetworkTileSource *
get_sat_source (ChamplainRenderer *renderer)
{
  return champlain_network_tile_source_new_full (SAT_SOURCE_ID,
    SAT_SOURCE_NAME,
    SAT_SOURCE_LICENCE,
    SAT_SOURCE_LICENCE_URI,
    SAT_SOURCE_MIN_ZOOM,
    SAT_SOURCE_MAX_ZOOM,
    TILE_SIZE,
    CHAMPLAIN_MAP_PROJECTION_MERCATOR,
    SAT_SOURCE_URI,
    renderer);
}

static ChamplainNetworkWmsTileSource *
get_geol_l_source (ChamplainRenderer *renderer)
{
  return champlain_network_wms_tile_source_new_full (GEO_SRC_LARGE_ID,
    GEO_SRC_LARGE_NAME,
    GEO_SRC_LARGE_LICENCE,
    GEO_SRC_LARGE_LICENCE_URI,
    GEO_SRC_LARGE_MIN_ZOOM,
    GEO_SRC_LARGE_MAX_ZOOM,
    TILE_SIZE,
    CHAMPLAIN_MAP_PROJECTION_MERCATOR,
    GEO_SRC_LARGE_URI,
    renderer);
}

static ChamplainNetworkWmsTileSource *
get_geol_s_source (ChamplainRenderer *renderer)
{
  return champlain_network_wms_tile_source_new_full (GEO_SRC_SMALL_ID,
    GEO_SRC_SMALL_NAME,
    GEO_SRC_SMALL_LICENCE,
    GEO_SRC_SMALL_LICENCE_URI,
    GEO_SRC_SMALL_MIN_ZOOM,
    GEO_SRC_SMALL_MAX_ZOOM,
    TILE_SIZE,
    CHAMPLAIN_MAP_PROJECTION_MERCATOR,
    GEO_SRC_SMALL_URI,
    renderer);
}

static ChamplainNetworkWmsTileSource *
get_mag_source (ChamplainRenderer *renderer)
{
  return champlain_network_wms_tile_source_new_full (MAG_SRC_ID,
    MAG_SRC_NAME,
    MAG_SRC_LICENCE,
    MAG_SRC_LICENCE_URI,
    MAG_SRC_MIN_ZOOM,
    MAG_SRC_MAX_ZOOM,
    TILE_SIZE,
    CHAMPLAIN_MAP_PROJECTION_MERCATOR,
    MAG_SRC_URI,
    renderer);
}

static ChamplainNetworkWmsTileSource *
get_grav_source (ChamplainRenderer *renderer)
{
  return champlain_network_wms_tile_source_new_full (GRAV_SRC_ID,
    GRAV_SRC_NAME,
    GRAV_SRC_LICENCE,
    GRAV_SRC_LICENCE_URI,
    GRAV_SRC_MIN_ZOOM,
    GRAV_SRC_MAX_ZOOM,
    TILE_SIZE,
    CHAMPLAIN_MAP_PROJECTION_MERCATOR,
    GRAV_SRC_URI,
    renderer);
}

static ChamplainNetworkWmsTileSource *
get_linear_source (ChamplainRenderer *renderer)
{
  return champlain_network_wms_tile_source_new_full (LINEAR_SRC_ID,
    LINEAR_SRC_NAME,
    LINEAR_SRC_LICENCE,
    LINEAR_SRC_LICENCE_URI,
    LINEAR_SRC_MIN_ZOOM,
    LINEAR_SRC_MAX_ZOOM,
    TILE_SIZE,
    CHAMPLAIN_MAP_PROJECTION_MERCATOR,
    LINEAR_SRC_URI,
    renderer);
}

static void
on_map_layer_relocated (ChamplainView *view, GrockMap *map)
{
  gint old_level = grock_map_get_zoom_level (map);
  gint current_level = grock_map_get_current_zoom_level (map);

  if ((old_level < current_level) && current_level == 12)
    {
      // zoomed in across the 11/12 boundary
      grock_map_remove_overlay_s (map);
      grock_map_set_overlay_l (map);
    }

  else if ((old_level > current_level) && current_level == 11)
    {
      // zoomed out across the 11/12 boundary
      grock_map_remove_overlay_l (map);
      grock_map_set_overlay_s (map);
    }

  grock_map_update_zoom_level (map);
}

static void
init_streets_source (GrockMap *self)
{
  g_assert (CHAMPLAIN_IS_IMAGE_RENDERER (self->renderer));

  self->streets_src = get_streets_source (CHAMPLAIN_RENDERER (self->renderer));

  self->streets_src_file_cache =  champlain_file_cache_new_full    (TILE_CACHE_SIZE_DISK, CACHE_PATH, CHAMPLAIN_RENDERER (self->renderer));
  self->streets_src_memy_cache =  champlain_memory_cache_new_full  (TILE_CACHE_SIZE_MEMY, CHAMPLAIN_RENDERER (self->renderer));

  g_assert (CHAMPLAIN_IS_NULL_TILE_SOURCE (self->errr_src));

  self->streets_src_chain = champlain_map_source_chain_new ();
  champlain_map_source_chain_push (self->streets_src_chain, CHAMPLAIN_MAP_SOURCE (self->errr_src));
  champlain_map_source_chain_push (self->streets_src_chain, CHAMPLAIN_MAP_SOURCE (self->streets_src));
  champlain_map_source_chain_push (self->streets_src_chain, CHAMPLAIN_MAP_SOURCE (self->streets_src_file_cache));
  champlain_map_source_chain_push (self->streets_src_chain, CHAMPLAIN_MAP_SOURCE (self->streets_src_memy_cache));
}

static void
init_sat_source (GrockMap *self)
{
  g_assert (CHAMPLAIN_IS_IMAGE_RENDERER (self->renderer));

  self->sat_src   = get_sat_source (CHAMPLAIN_RENDERER (self->renderer));

  self->sat_src_file_cache =  champlain_file_cache_new_full    (TILE_CACHE_SIZE_DISK, CACHE_PATH, CHAMPLAIN_RENDERER (self->renderer));
  self->sat_src_memy_cache =  champlain_memory_cache_new_full  (TILE_CACHE_SIZE_MEMY, CHAMPLAIN_RENDERER (self->renderer));

  g_assert (CHAMPLAIN_IS_NULL_TILE_SOURCE (self->errr_src));

  self->sat_src_chain = champlain_map_source_chain_new ();
  champlain_map_source_chain_push (self->sat_src_chain, CHAMPLAIN_MAP_SOURCE (self->errr_src));
  champlain_map_source_chain_push (self->sat_src_chain, CHAMPLAIN_MAP_SOURCE (self->sat_src));
  champlain_map_source_chain_push (self->sat_src_chain, CHAMPLAIN_MAP_SOURCE (self->sat_src_file_cache));
  champlain_map_source_chain_push (self->sat_src_chain, CHAMPLAIN_MAP_SOURCE (self->sat_src_memy_cache));
}

static void
init_geol_s_source (GrockMap *self)
{
  g_assert (CHAMPLAIN_IS_IMAGE_RENDERER (self->renderer));

  self->geol_src_s   = get_geol_s_source (CHAMPLAIN_RENDERER (self->renderer));

  self->geol_src_s_file_cache =  champlain_file_cache_new_full    (TILE_CACHE_SIZE_DISK, CACHE_PATH, CHAMPLAIN_RENDERER (self->renderer));
  self->geol_src_s_memy_cache =  champlain_memory_cache_new_full  (TILE_CACHE_SIZE_MEMY, CHAMPLAIN_RENDERER (self->renderer));

  g_assert (CHAMPLAIN_IS_NULL_TILE_SOURCE (self->errr_src));

  self->geol_src_s_chain = champlain_map_source_chain_new ();
  champlain_map_source_chain_push (self->geol_src_s_chain, CHAMPLAIN_MAP_SOURCE (self->errr_src));
  champlain_map_source_chain_push (self->geol_src_s_chain, CHAMPLAIN_MAP_SOURCE (self->geol_src_s));
  champlain_map_source_chain_push (self->geol_src_s_chain, CHAMPLAIN_MAP_SOURCE (self->geol_src_s_file_cache));
  champlain_map_source_chain_push (self->geol_src_s_chain, CHAMPLAIN_MAP_SOURCE (self->geol_src_s_memy_cache));
}

static void
init_geol_l_source (GrockMap *self)
{
  g_assert (CHAMPLAIN_IS_IMAGE_RENDERER (self->renderer));

  self->geol_src_l = get_geol_l_source (CHAMPLAIN_RENDERER (self->renderer));

  self->geol_src_l_file_cache =  champlain_file_cache_new_full    (TILE_CACHE_SIZE_DISK, CACHE_PATH, CHAMPLAIN_RENDERER (self->renderer));
  self->geol_src_l_memy_cache =  champlain_memory_cache_new_full  (TILE_CACHE_SIZE_MEMY, CHAMPLAIN_RENDERER (self->renderer));

  g_assert (CHAMPLAIN_IS_NULL_TILE_SOURCE (self->errr_src));

  self->geol_src_l_chain = champlain_map_source_chain_new ();
  champlain_map_source_chain_push (self->geol_src_l_chain, CHAMPLAIN_MAP_SOURCE (self->errr_src));
  champlain_map_source_chain_push (self->geol_src_l_chain, CHAMPLAIN_MAP_SOURCE (self->geol_src_l));
  champlain_map_source_chain_push (self->geol_src_l_chain, CHAMPLAIN_MAP_SOURCE (self->geol_src_l_file_cache));
  champlain_map_source_chain_push (self->geol_src_l_chain, CHAMPLAIN_MAP_SOURCE (self->geol_src_l_memy_cache));
}

static void
init_mag_source (GrockMap *self)
{
  g_assert (CHAMPLAIN_IS_IMAGE_RENDERER (self->renderer));

  self->mag_src = get_mag_source (CHAMPLAIN_RENDERER (self->renderer));

  self->mag_src_file_cache =  champlain_file_cache_new_full    (TILE_CACHE_SIZE_DISK, CACHE_PATH, CHAMPLAIN_RENDERER (self->renderer));
  self->mag_src_memy_cache =  champlain_memory_cache_new_full  (TILE_CACHE_SIZE_MEMY, CHAMPLAIN_RENDERER (self->renderer));

  g_assert (CHAMPLAIN_IS_NULL_TILE_SOURCE (self->errr_src));

  self->mag_src_chain = champlain_map_source_chain_new ();
  champlain_map_source_chain_push (self->mag_src_chain, CHAMPLAIN_MAP_SOURCE (self->errr_src));
  champlain_map_source_chain_push (self->mag_src_chain, CHAMPLAIN_MAP_SOURCE (self->mag_src));
  champlain_map_source_chain_push (self->mag_src_chain, CHAMPLAIN_MAP_SOURCE (self->mag_src_file_cache));
  champlain_map_source_chain_push (self->mag_src_chain, CHAMPLAIN_MAP_SOURCE (self->mag_src_memy_cache));
}

static void
init_grav_source (GrockMap *self)
{
  g_assert (CHAMPLAIN_IS_IMAGE_RENDERER (self->renderer));

  self->grav_src = get_grav_source (CHAMPLAIN_RENDERER (self->renderer));

  self->grav_src_file_cache =  champlain_file_cache_new_full    (TILE_CACHE_SIZE_DISK, CACHE_PATH, CHAMPLAIN_RENDERER (self->renderer));
  self->grav_src_memy_cache =  champlain_memory_cache_new_full  (TILE_CACHE_SIZE_MEMY, CHAMPLAIN_RENDERER (self->renderer));

  g_assert (CHAMPLAIN_IS_NULL_TILE_SOURCE (self->errr_src));

  self->grav_src_chain = champlain_map_source_chain_new ();

  champlain_map_source_chain_push (self->grav_src_chain, CHAMPLAIN_MAP_SOURCE (self->errr_src));
  champlain_map_source_chain_push (self->grav_src_chain, CHAMPLAIN_MAP_SOURCE (self->grav_src));
  champlain_map_source_chain_push (self->grav_src_chain, CHAMPLAIN_MAP_SOURCE (self->grav_src_file_cache));
  champlain_map_source_chain_push (self->grav_src_chain, CHAMPLAIN_MAP_SOURCE (self->grav_src_memy_cache));
}

static void
init_linear_source (GrockMap *self)
{
  g_assert (CHAMPLAIN_IS_IMAGE_RENDERER (self->renderer));

  self->linear_src = get_linear_source (CHAMPLAIN_RENDERER (self->renderer));

  self->linear_src_file_cache =  champlain_file_cache_new_full    (TILE_CACHE_SIZE_DISK, CACHE_PATH, CHAMPLAIN_RENDERER (self->renderer));
  self->linear_src_memy_cache =  champlain_memory_cache_new_full  (TILE_CACHE_SIZE_MEMY, CHAMPLAIN_RENDERER (self->renderer));

  g_assert (CHAMPLAIN_IS_NULL_TILE_SOURCE (self->errr_src));

  self->linear_src_chain = champlain_map_source_chain_new ();

  champlain_map_source_chain_push (self->linear_src_chain, CHAMPLAIN_MAP_SOURCE (self->errr_src));
  champlain_map_source_chain_push (self->linear_src_chain, CHAMPLAIN_MAP_SOURCE (self->linear_src));
  champlain_map_source_chain_push (self->linear_src_chain, CHAMPLAIN_MAP_SOURCE (self->linear_src_file_cache));
  champlain_map_source_chain_push (self->linear_src_chain, CHAMPLAIN_MAP_SOURCE (self->linear_src_memy_cache));
}

static void
grock_map_finalize (GObject *object)
{
  GrockMap *self = GROCK_MAP (object);

  champlain_bounding_box_free (self->bbox);

  G_OBJECT_CLASS (grock_map_parent_class)->finalize (object);
}

static void
grock_map_class_init (GrockMapClass *class)
{
  GObjectClass *object_class = G_OBJECT_CLASS (class);

  object_class->finalize = grock_map_finalize;
}

static void
grock_map_init (GrockMap *self)
{
  self->renderer = champlain_image_renderer_new ();
  self->error_renderer = champlain_error_tile_renderer_new (TILE_SIZE);

  self->errr_src = champlain_null_tile_source_new_full (CHAMPLAIN_RENDERER (self->error_renderer));

  init_streets_source (self);
  init_sat_source (self);
  init_geol_l_source (self);
  init_geol_s_source (self);
  init_grav_source (self);
  init_mag_source (self);
  init_linear_source (self);

  self->current_overlay = NULL;
  self->current_main_source = NULL;

  self->map_embed = GTK_CHAMPLAIN_EMBED (gtk_champlain_embed_new ());
  gtk_widget_add_events (GTK_WIDGET (self->map_embed), GDK_BUTTON_RELEASE);

  self->view = gtk_champlain_embed_get_view (self->map_embed);

  self->scale = champlain_scale_new ();
  champlain_scale_connect_view (CHAMPLAIN_SCALE (self->scale), self->view);

  /* align scale to bottom left */
  clutter_actor_set_x_expand (self->scale, TRUE);
  clutter_actor_set_y_expand (self->scale, TRUE);
  clutter_actor_set_x_align (self->scale, CLUTTER_ACTOR_ALIGN_START);
  clutter_actor_set_y_align (self->scale, CLUTTER_ACTOR_ALIGN_END);
  clutter_actor_add_child (CLUTTER_ACTOR (self->view), self->scale);

  clutter_actor_set_reactive (CLUTTER_ACTOR (self->view), TRUE);

  self->licence_actor = champlain_view_get_license_actor (self->view);
  self->zoom_level = champlain_view_get_zoom_level (self->view);

  self->overlay_opacity = GROCK_INITIAL_OVERLAY_OPACITY;

  self->bbox = champlain_bounding_box_new ();
  champlain_bounding_box_extend (self->bbox, 49.846369, -8.344021);
  champlain_bounding_box_extend (self->bbox, 60.866269, 2.697705);
  champlain_view_set_world (self->view, self->bbox);

  g_signal_connect (self->view, "layer-relocated",
                    G_CALLBACK (on_map_layer_relocated),
                    self);

  g_object_set (G_OBJECT (self->view), "kinetic-mode", TRUE,
                                       "zoom-level", GROCK_INITIAL_ZOOM_LEVEL,
                                       NULL);

  self->mag_is_active = FALSE;
  self->grav_is_active = FALSE;
  self->geol_l_is_active = FALSE;
  self->geol_s_is_active = FALSE;
  self->linear_is_active = FALSE;
}

GtkChamplainEmbed *
grock_map_get_map_embed (GrockMap *self)
{
  return self->map_embed;
}

void
grock_map_print_current_overlays (GrockMap* self)
{
  GList *overlays = champlain_view_get_overlay_sources (self->view);
  GList *l;

  for (l = overlays; l != NULL; l = l->next)
    {
       g_print ("Overlay: %s\n", champlain_map_source_get_name (l->data));
    }

  g_print ("\n");
}

/*
static void
set_overlay_source_chain (GrockMap *self, ChamplainMapSourceChain *src,
                          gint opacity)
{
  g_assert (CHAMPLAIN_IS_MAP_SOURCE_CHAIN (src));

  GList *current_overlays = champlain_view_get_overlay_sources (self->view);

  // confirm that only one overlay is active, or none at all
  g_assert (g_list_length (current_overlays) <= 1);

  if (g_list_length (current_overlays) == 1) // if we do have a geology overlay, remove it
    champlain_view_remove_overlay_source (self->view, current_overlays->data);

  current_overlays = champlain_view_get_overlay_sources (self->view);
  g_assert (current_overlays == NULL); // no overlays at the moment

  champlain_view_add_overlay_source (self->view, CHAMPLAIN_MAP_SOURCE (src),
                                     opacity);

  self->current_overlay = CHAMPLAIN_MAP_SOURCE (src);
} */

void
grock_map_finish (GrockMap *self)
{
  // Center on the greatest city in the world
  champlain_view_center_on (self->view, 53.382298, -1.468386);

  champlain_view_set_zoom_level (self->view, GROCK_INITIAL_ZOOM_LEVEL);

  gtk_widget_set_size_request (GTK_WIDGET (self->map_embed), 640, 481);

}

gdouble
grock_map_get_lat (GrockMap *self)
{
  return champlain_view_get_center_latitude (self->view);
}

gdouble
grock_map_get_lon (GrockMap *self)
{
  return champlain_view_get_center_longitude (self->view);
}

/*
 * Returns the zoom level stored in the zoom level variable.
 * After zooming, the actual zoom level and the zoom level variable will
 * be different since the variable has yet to be updated. The
 * on_map_layer_relocated callback in grock-window.c uses this to
 * determine the direction of zooming.
 */
gint
grock_map_get_zoom_level (GrockMap *self)
{
  return self->zoom_level;
}

/* Returns the actual zoom level of the view. */
gint
grock_map_get_current_zoom_level (GrockMap *self)
{
  return champlain_view_get_zoom_level (self->view);
}

void
grock_map_update_zoom_level (GrockMap *self)
{
  self->zoom_level = champlain_view_get_zoom_level (self->view);
}

void
grock_map_set_zoom_level (GrockMap *self, gint zoom_level)
{
  champlain_view_set_zoom_level (self->view, zoom_level);

  grock_map_update_zoom_level (self);
}

void
grock_map_zoom_in (GrockMap *self)
{
  champlain_view_zoom_in (self->view);
}

void
grock_map_zoom_out (GrockMap *self)
{
  champlain_view_zoom_out (self->view);
}

void
grock_map_go_to (GrockMap *self, gdouble lat, gdouble lon)
{
  champlain_view_go_to (self->view, lat, lon);
}

void
grock_map_center_on (GrockMap *self, gdouble lat, gdouble lon)
{
  champlain_view_center_on (self->view, lat, lon);
}

void
grock_map_set_overlay_l (GrockMap *self)
{
  g_assert (CHAMPLAIN_IS_MAP_SOURCE_CHAIN (self->geol_src_l_chain));

  champlain_view_add_overlay_source (self->view,
                                     CHAMPLAIN_MAP_SOURCE (self->geol_src_l_chain),
                                     self->overlay_opacity);

  self->geol_l_is_active = TRUE;
  self->current_overlay = CHAMPLAIN_MAP_SOURCE (self->geol_src_l_chain);
}

void
grock_map_remove_overlay_l (GrockMap *self)
{
  champlain_view_remove_overlay_source (self->view, CHAMPLAIN_MAP_SOURCE (self->current_overlay));
  self->geol_l_is_active = FALSE;
  self->current_overlay = NULL;
}

void
grock_map_set_overlay_s (GrockMap *self)
{
  g_assert (CHAMPLAIN_IS_MAP_SOURCE_CHAIN (self->geol_src_s_chain));
  champlain_view_add_overlay_source (self->view,
                                     CHAMPLAIN_MAP_SOURCE (self->geol_src_s_chain),
                                     self->overlay_opacity);
  self->geol_s_is_active = TRUE;
  self->current_overlay = CHAMPLAIN_MAP_SOURCE (self->geol_src_s_chain);
}

void
grock_map_remove_overlay_s (GrockMap *self)
{
  champlain_view_remove_overlay_source (self->view, CHAMPLAIN_MAP_SOURCE (self->current_overlay));
  self->geol_s_is_active = FALSE;
  self->current_overlay = NULL;
}

void
grock_map_set_overlay_mag (GrockMap *self)
{
  g_assert (CHAMPLAIN_IS_MAP_SOURCE_CHAIN (self->mag_src_chain));
  champlain_view_add_overlay_source (self->view, CHAMPLAIN_MAP_SOURCE (self->mag_src_chain), 100);
  self->mag_is_active = TRUE;
}

void
grock_map_remove_overlay_mag (GrockMap *self)
{
  champlain_view_remove_overlay_source (self->view, CHAMPLAIN_MAP_SOURCE (self->mag_src_chain));
  self->mag_is_active = FALSE;
}

void
grock_map_set_overlay_grav (GrockMap *self)
{
  g_assert (CHAMPLAIN_IS_MAP_SOURCE (self->grav_src_chain));
  champlain_view_add_overlay_source (self->view, CHAMPLAIN_MAP_SOURCE (self->grav_src_chain), 100);
  self->grav_is_active = TRUE;
}

void
grock_map_remove_overlay_grav (GrockMap *self)
{
  champlain_view_remove_overlay_source (self->view, CHAMPLAIN_MAP_SOURCE (self->grav_src_chain));
  self->grav_is_active = FALSE;
}

void
grock_map_set_overlay_linear (GrockMap *self)
{
  init_linear_source (self);
  g_assert (CHAMPLAIN_IS_MAP_SOURCE (self->linear_src_chain));

  champlain_view_add_overlay_source (self->view,
                                     CHAMPLAIN_MAP_SOURCE (self->linear_src_chain),
                                     100);

  self->linear_is_active = TRUE;
}

void
grock_map_remove_overlay_linear (GrockMap *self)
{
  champlain_view_remove_overlay_source (self->view, CHAMPLAIN_MAP_SOURCE (self->linear_src_chain));
  self->linear_is_active = FALSE;
}

void
grock_map_update_overlay_opacity (GrockMap *self, gint opacity)
{
  //g_assert (CHAMPLAIN_IS_MAP_SOURCE (self->current_overlay));

  self->overlay_opacity = opacity;

  if (self->linear_is_active)
    {
      grock_map_remove_overlay_linear (self);
      grock_map_set_overlay_linear (self);
    }

  if (self->geol_s_is_active)
    {
      grock_map_remove_overlay_s (self);
      grock_map_set_overlay_s (self);
    }
  else if (self->geol_l_is_active)
    {
      grock_map_remove_overlay_l (self);
      grock_map_set_overlay_l (self);
    }
  else
    g_error ("No overlay is active.\n");
}

void
grock_map_set_window (GrockMap *self, GrockWindow *window)
{
  g_object_set_data (G_OBJECT (self), "window", window);
}

void
grock_map_add_to_container (GrockMap *self, GtkContainer *container)
{
  gtk_container_add (container, GTK_WIDGET (self->map_embed));
}

void
grock_map_show (GrockMap *self)
{
  gtk_widget_show_all (GTK_WIDGET (self->map_embed));
}

void
grock_map_set_visible_stack_child (GrockMap *self, GtkStack *stack)
{
  gtk_stack_set_visible_child (stack, GTK_WIDGET (self->map_embed));
}

void
grock_map_set_main_source_sat (GrockMap *self)
{
  // Check that the main 'streets' source is indeed currently displayed or we have no source at all
  if (self->current_main_source != NULL)
    g_assert (!g_strcmp0 (champlain_map_source_get_id (self->current_main_source), STREETS_SOURCE_ID));

  /**
   * Need to re-initialise since resetting the map source calls
   * g_object_unref() on the source.
   */
  init_sat_source (self);
  champlain_view_set_map_source (self->view, CHAMPLAIN_MAP_SOURCE (self->sat_src_chain));
  self->current_main_source = champlain_view_get_map_source (self->view);

  /**
   * Resetting the main source removes the overlays, but the rest of the
   * program expects them to be present, so we must re-add them now.
   */
  ChamplainMapSource *current = self->current_overlay;

  if (current == NULL)
    return;
  if (!g_strcmp0 (champlain_map_source_get_name (current), GEO_SRC_LARGE_NAME))
    grock_map_set_overlay_l (self);
  else if (!g_strcmp0 (champlain_map_source_get_name (current), GEO_SRC_SMALL_NAME))
    grock_map_set_overlay_s (self);
}

void
grock_map_set_main_source_streets (GrockMap *self)
{
  // Check that the main 'sat' source is indeed currently displayed or we have no source at all
  if (self->current_main_source != NULL)
    g_assert (!g_strcmp0 (champlain_map_source_get_id (self->current_main_source), SAT_SOURCE_ID));

  /**
   * Need to re-initialise since resetting the map source calls
   * g_object_unref() on the source.
   */
  init_streets_source (self);
  champlain_view_set_map_source (self->view, CHAMPLAIN_MAP_SOURCE (self->streets_src_chain));
  self->current_main_source = champlain_view_get_map_source (self->view);

  /**
   * Resetting the main source removes the overlays, but the rest of the
   * program expects them to be present, so we must re-add them now.
   */
  ChamplainMapSource *current = self->current_overlay;
  if (current == NULL)
    return;
  if (!g_strcmp0 (champlain_map_source_get_name (current), GEO_SRC_LARGE_NAME))
    grock_map_set_overlay_l (self);
  else if (!g_strcmp0 (champlain_map_source_get_name (current), GEO_SRC_SMALL_NAME))
    grock_map_set_overlay_s (self);
}

void
grock_map_remove_overlays (GrockMap *self)
{
  if (self->current_overlay != NULL)
    {
      champlain_view_remove_overlay_source (self->view, self->current_overlay);
      self->current_overlay = NULL;
    }

  GList *l = champlain_view_get_overlay_sources (self->view);
  if (l != NULL)
    g_critical ("Did not remove all overlay sources. More than one overlay must have been active.\n");

}

GrockMap *
grock_map_new (void)
{
  return g_object_new (GROCK_TYPE_MAP, NULL);
}
