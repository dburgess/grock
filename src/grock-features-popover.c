/* grock-features-popover.c
 *
 * Copyright © 2019 Daniel Burgess
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "grock-features.h"
#include "grock-features-popover.h"

#include <gtk-3.0/gtk/gtk.h>

/**
 * Takes an ASCII string and makes the first letter of the first word
 * upper case.
 */
static gchar *
cap_first_letter (gchar *str)
{
  gchar *c = str;
  *c = g_ascii_toupper (*c);

  return str;
}

/**
 * Takes an ASCII string and ensures that the first letter of every word is
 * upper case and all other letters are lower case.
 */
static gchar *
cap_first_letter_each (gchar *str)
{
  gchar *c = str;
  *c = g_ascii_toupper (*c);
  ++c;

  while (*c != '\0')
    {
      if (*(c - 1) == ' ')
        {
          ++c;
          continue;
        }
      *c = g_ascii_tolower (*c);
      ++c;
    }

  return str;
}

static void
info_prettify (GrockFeatureInfo *i)
{
  /**
   * Prettify the heading: the section before the hyphen is the formation name
   * and should have the first letter of each word capitalised. The section
   * after the hyphen is the type of rock, and should be all lower case.
   */
  gchar *c = i->heading;
  ++c; // Skip first letter, we need that capitalised
  while (*c != '-' && *c != '\0')
    {
      if (*(c - 1) == ' ')   // Keep the first letter in the word capitalised
        {
          ++c;
          continue;
        }
      *c = g_ascii_tolower (*c);
      ++c;
    }
  while (*c != '\0')
    {
      *c = g_ascii_tolower (*c);
      ++c;
    }

  cap_first_letter_each (i->min_specific_time);
  cap_first_letter_each (i->max_specific_time);
  cap_first_letter_each (i->min_period);
  cap_first_letter_each (i->max_period);
  cap_first_letter (i->setting);
  cap_first_letter (i->setting_plus);

  // If setting_plus has something interesting, add it to setting.
  if (g_strcmp0 (i->setting_plus, "Null") &&
      g_strcmp0 (i->setting_plus, "NULL"))
        i->setting = g_strconcat (i->setting, ". ", i->setting_plus, ".", NULL);
  else  // Otherwise just add a full stop.
        i->setting = g_strconcat (i->setting, ". ", NULL);

}

static void
popover_set_info_data (GrockFeaturesPopover *p, GrockFeatureInfo *info)
{
  gchar *specific_time_str;
  gchar *period_str;

  const gchar *heading_format = "<span size='large' weight='bold'>\%s</span>";
  const gchar *heading_markup = g_markup_printf_escaped (heading_format, info->heading);
  gtk_label_set_markup (GTK_LABEL (p->lbl_heading), heading_markup);

  const gchar *coords_format = "<span size='small' foreground='gray'>   %s</span>";
  const gchar *coords_markup = g_markup_printf_escaped (coords_format, info->bng_string);
  gtk_label_set_markup (GTK_LABEL (p->lbl_coords), coords_markup);

  if (!g_strcmp0 (info->min_specific_time, info->max_specific_time)) // if they're the same
    specific_time_str = g_strconcat (info->min_specific_time, NULL);
  else
    specific_time_str = g_strconcat (info->min_specific_time, " to ", info->max_specific_time, NULL);

  if (!g_strcmp0 (info->min_period, info->max_period)) // if they're the same
    period_str = g_strconcat (info->min_period, NULL);
  else
    period_str = g_strconcat (info->min_period, " to ", info->min_period, NULL);

  gchar *total_period_str = g_strconcat (period_str, " (", specific_time_str, ")", NULL);

  gtk_label_set_text (GTK_LABEL (p->lbl_period), total_period_str);

  gtk_label_set_text (GTK_LABEL (p->lbl_rock_type), info->rock_type);
  gtk_label_set_text (GTK_LABEL (p->lbl_setting), info->setting);
  gtk_label_set_text (GTK_LABEL (p->lbl_setting_plus), info->setting_plus);
  gtk_label_set_text (GTK_LABEL (p->lbl_environment), info->environment);
}

void
grock_features_popover_display_info (GrockFeaturesPopover *p)
{
  GrockFeatureInfo *i = p->info;

  info_prettify (i);

  popover_set_info_data (p, p->info);
  gtk_stack_set_visible_child (GTK_STACK (p->stack), p->data_grid);
}

static void
on_popover_closed (GtkPopover *popover, GrockFeaturesPopover *pop)
{
  g_free (pop->info->bng_string);
  g_free (pop->info->heading);
  g_free (pop->info->min_specific_time);
  g_free (pop->info->max_specific_time);
  g_free (pop->info->min_period);
  g_free (pop->info->max_period);
  g_free (pop->info->rock_type);
  g_free (pop->info->setting);
  g_free (pop->info->environment);
}

static GrockFeaturesPopover *
popover_create (GtkWidget *widget)
{
  static GrockFeaturesPopover ret;

  ret.info = grock_feature_info_new ();
  ret.pop = gtk_popover_new (widget);

  g_signal_connect (ret.pop, "closed", G_CALLBACK (on_popover_closed), &ret);

  g_assert (GTK_IS_POPOVER (ret.pop));

  ret.main_grid = gtk_grid_new ();

  ret.stack = gtk_stack_new ();
  ret.spin_box = gtk_box_new (GTK_ORIENTATION_VERTICAL, 20);
  ret.data_grid = gtk_grid_new ();

  ret.spinner = gtk_spinner_new ();

  gtk_widget_set_valign (ret.spin_box, GTK_ALIGN_CENTER);


  ret.lbl_empty = gtk_label_new ("                                         ");
  ret.lbl_lat = gtk_label_new ("NULL");
  ret.lbl_lon = gtk_label_new ("NULL");
  ret.lbl_heading = gtk_label_new ("NULL");
  ret.lbl_period = gtk_label_new ("NULL");
  ret.lbl_rock_type = gtk_label_new ("NULL");
  ret.lbl_setting = gtk_label_new ("NULL");
  ret.lbl_setting_plus = gtk_label_new ("NULL");
  ret.lbl_environment = gtk_label_new ("NULL");

  ret.lbl_coords = gtk_label_new ("NULL");

  gint char_width = 30;

  gtk_label_set_line_wrap (GTK_LABEL (ret.lbl_heading), TRUE);
  gtk_label_set_max_width_chars (GTK_LABEL (ret.lbl_heading), char_width);
  gtk_label_set_line_wrap (GTK_LABEL (ret.lbl_period), TRUE);
  gtk_label_set_max_width_chars (GTK_LABEL (ret.lbl_period), char_width);
  gtk_label_set_line_wrap (GTK_LABEL (ret.lbl_rock_type), TRUE);
  gtk_label_set_max_width_chars (GTK_LABEL (ret.lbl_rock_type), char_width);
  gtk_label_set_line_wrap (GTK_LABEL (ret.lbl_setting), TRUE);
  gtk_label_set_max_width_chars (GTK_LABEL (ret.lbl_setting), char_width);
  gtk_label_set_line_wrap (GTK_LABEL (ret.lbl_setting_plus), TRUE);
  gtk_label_set_max_width_chars (GTK_LABEL (ret.lbl_setting_plus), char_width);
  gtk_label_set_line_wrap (GTK_LABEL (ret.lbl_environment), TRUE);
  gtk_label_set_max_width_chars (GTK_LABEL (ret.lbl_environment), char_width);
  gtk_label_set_justify (GTK_LABEL (ret.lbl_environment), GTK_JUSTIFY_FILL);

  gtk_widget_set_halign (GTK_WIDGET (ret.lbl_coords), GTK_ALIGN_START);

  gtk_label_set_selectable (GTK_LABEL (ret.lbl_heading), TRUE);
  gtk_label_set_selectable (GTK_LABEL (ret.lbl_period), TRUE);
  gtk_label_set_selectable (GTK_LABEL (ret.lbl_setting), TRUE);
  gtk_label_set_selectable (GTK_LABEL (ret.lbl_environment), TRUE);
  gtk_label_set_selectable (GTK_LABEL (ret.lbl_coords), TRUE);

  return &ret;
}

/**
 * TODO: using blank labels here as padding is a horrible hack. Find
 * another way to do this (probably involves custom CSS).
 */
static void
popover_populate (GrockFeaturesPopover *p)
{
  gtk_box_pack_end (GTK_BOX (p->spin_box), p->lbl_empty, FALSE, FALSE, 0);

  GtkWidget *lbl_blank_left = gtk_label_new (NULL);
  gtk_label_set_markup (GTK_LABEL (lbl_blank_left), "<span size='xx-small'>     </span>");
  gtk_grid_attach (GTK_GRID (p->data_grid), lbl_blank_left, 1, 1, 1, 4);
  GtkWidget *lbl_blank_top = gtk_label_new (NULL);
  gtk_label_set_markup (GTK_LABEL (lbl_blank_top), "<span size='xx-small'> </span>");
  gtk_grid_attach (GTK_GRID (p->data_grid), lbl_blank_top, 2, 1, 3, 1);
  GtkWidget *lbl_blank_right = gtk_label_new (NULL);
  gtk_label_set_markup (GTK_LABEL (lbl_blank_right), "<span size='xx-small'>     </span>");
  gtk_grid_attach (GTK_GRID (p->data_grid), lbl_blank_right, 5, 1, 1, 4);

  gtk_label_set_selectable (GTK_LABEL (p->lbl_heading), TRUE);
  gtk_label_set_selectable (GTK_LABEL (p->lbl_period), TRUE);
  gtk_label_set_selectable (GTK_LABEL (p->lbl_setting), TRUE);
  gtk_label_set_selectable (GTK_LABEL (p->lbl_environment), TRUE);
  gtk_label_set_selectable (GTK_LABEL (p->lbl_coords), TRUE);

  gtk_popover_set_modal (GTK_POPOVER (p->pop), TRUE);

  gtk_grid_attach (GTK_GRID (p->data_grid), p->lbl_heading, 2, 2, 3, 1);
  gtk_grid_attach (GTK_GRID (p->data_grid), p->lbl_period, 2, 3, 1, 1);
  gtk_grid_attach (GTK_GRID (p->data_grid), p->lbl_setting, 2, 4, 1, 1);
  gtk_grid_attach (GTK_GRID (p->data_grid), p->lbl_environment, 4, 3, 1, 2);

  gtk_grid_attach (GTK_GRID (p->data_grid), p->lbl_coords, 1, 5, 5, 1);

  gtk_grid_set_row_spacing (GTK_GRID (p->data_grid), 10);
  gtk_grid_set_column_spacing (GTK_GRID (p->data_grid), 10);

  gtk_box_pack_start (GTK_BOX (p->spin_box), p->spinner, FALSE, FALSE, 20);

  gtk_stack_add_named (GTK_STACK (p->stack), p->data_grid, "data_grid");
  gtk_stack_add_named (GTK_STACK (p->stack), p->spin_box, "spin_box");

  gtk_grid_attach (GTK_GRID (p->main_grid), p->stack, 1, 1, 1, 1);

  gtk_container_add (GTK_CONTAINER (p->pop), p->main_grid);

  gtk_widget_show_all (GTK_WIDGET (p->pop));

  gtk_stack_set_visible_child (GTK_STACK (p->stack), p->spin_box);
  gtk_stack_set_transition_type (GTK_STACK (p->stack), GTK_STACK_TRANSITION_TYPE_OVER_DOWN);
  gtk_spinner_start (GTK_SPINNER (p->spinner));
}


void
grock_features_popover_make (GtkWidget *widget, gdouble x, gdouble y)
{
  ChamplainView *view = gtk_champlain_embed_get_view (GTK_CHAMPLAIN_EMBED (widget));
  g_assert (CHAMPLAIN_IS_VIEW (view));

  GrockFeaturesPopover *p = popover_create (widget);

  GdkRectangle rect = {x, y, 8, 8};
  gtk_popover_set_pointing_to (GTK_POPOVER (p->pop), &rect);
  gtk_popover_set_position (GTK_POPOVER (p->pop), GTK_POS_BOTTOM);

  popover_populate (p);

  gdouble lon = champlain_view_x_to_longitude (view, x);
  gdouble lat = champlain_view_y_to_latitude (view, y);
  GrockPoint point = grock_features_get_web_mercators_from_coords (lat, lon);

  gtk_popover_popup (GTK_POPOVER (p->pop));

  // get the grid reference
  grock_features_get_bng_from_epsg3857 (point.lon, point.lat, p);
  // get the actual feature information
  grock_features_get_from_epsg3857 (point.lat, point.lon, p);
}


