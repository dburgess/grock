/* grock-window.h
 *
 * Copyright © 2019 Daniel Burgess
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GROCK_WINDOW_H
#define GROCK_WINDOW_H

#include <gtk/gtk.h>

#include "grock-application.h"

G_BEGIN_DECLS

#define GROCK_TYPE_WINDOW (grock_window_get_type())

G_DECLARE_FINAL_TYPE (GrockWindow, grock_window, GROCK, WINDOW, GtkApplicationWindow)

GrockWindow *grock_window_new               (GrockApplication *app);
void         grock_window_add_map           (GrockWindow *self);
void         grock_window_button_enable     (GtkButton *button);
void         grock_window_button_disable    (GtkButton *button);
void         grock_window_show_about_dialog (GrockWindow *window);

G_END_DECLS

#endif
