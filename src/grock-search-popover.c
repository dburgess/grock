/* grock-search-popover.c
 *
 * Copyright © 2019 Daniel Burgess
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "grock-search-popover.h"

#include "grock-search.h"

#include <geocode-glib-1.0/geocode-glib/geocode-glib.h>
#include <glib/gi18n.h>
#include <gtk-3.0/gtk/gtk.h>

/**
 * We need to propagate keypress events detected on the popover to the
 * GtkSearchEntry so it can get the keypresses without closing the
 * GtkPopover.
 */
static gboolean
on_key_press_popover (GtkWidget *popover, GdkEvent *event)
{
  GtkWidget *entry = gtk_popover_get_relative_to (GTK_POPOVER (popover));
  return gtk_search_entry_handle_event (GTK_SEARCH_ENTRY (entry), event);
}

static void
on_popover_closed (GtkPopover *popover, GrockSearchPopover *pop)
{
  g_debug ("Failed to clean up after search popover.\n");
}

static GrockSearchPopover *
popover_create (GtkWidget *widget)
{
  static GrockSearchPopover ret;

  ret.pop = gtk_popover_new (widget);

  g_signal_connect (ret.pop, "closed", G_CALLBACK (on_popover_closed), &ret);

  gint i = 0;
  while (i < 10)
    {
      ret.result_labels[i] = gtk_label_new ("NULL");
      ret.list_box_rows[i] = GTK_LIST_BOX_ROW (gtk_list_box_row_new ());
      gtk_container_add (GTK_CONTAINER (ret.list_box_rows[i]), ret.result_labels[i]);
      ++i;
    }

  ret.main_grid = gtk_grid_new ();

  ret.main_stack = gtk_stack_new ();

  ret.spinner = gtk_spinner_new ();
  ret.lbl_no_results = gtk_label_new (_("No results found.\n"));
  ret.scrolled_window = gtk_scrolled_window_new (NULL, NULL);

  gtk_scrolled_window_set_min_content_height (GTK_SCROLLED_WINDOW (ret.scrolled_window), 300);
  gtk_scrolled_window_set_min_content_width (GTK_SCROLLED_WINDOW (ret.scrolled_window), 500);

  ret.list_box = gtk_list_box_new ();

  for (gint i = 0; i < 10; ++i)
     gtk_list_box_insert (GTK_LIST_BOX (ret.list_box), GTK_WIDGET (ret.list_box_rows[i]), -1);

  gtk_container_add (GTK_CONTAINER (ret.scrolled_window), ret.list_box);

  gtk_stack_add_named (GTK_STACK (ret.main_stack), ret.spinner, "spinner");
  gtk_stack_add_named (GTK_STACK (ret.main_stack), ret.lbl_no_results, "no-results");
  gtk_stack_add_named (GTK_STACK (ret.main_stack), ret.scrolled_window, "results");

  gtk_stack_set_visible_child (GTK_STACK (ret.main_stack), ret.spinner);
  gtk_stack_set_transition_type (GTK_STACK (ret.main_stack), GTK_STACK_TRANSITION_TYPE_OVER_DOWN);
  gtk_spinner_start (GTK_SPINNER (ret.spinner));

  gtk_grid_attach (GTK_GRID (ret.main_grid), ret.main_stack, 1, 1, 1, 1);

  gtk_container_add (GTK_CONTAINER (ret.pop), ret.main_grid);
  gtk_widget_show_all (GTK_WIDGET (ret.pop));

  gtk_popover_set_position (GTK_POPOVER (ret.pop), GTK_POS_BOTTOM);

  return &ret;
}

// TODO: find a better way to
void
grock_search_popover_populate (GrockSearchPopover *p, GList *places_list)
{
  gint i = 0;
  GList *l = places_list;

  while (i < 10)
    {
      g_assert (i < 10);
      if (l == NULL)
        {
          gtk_label_set_text (GTK_LABEL (p->result_labels[i]), "");
          gtk_widget_set_visible (GTK_WIDGET (p->list_box_rows[i]), FALSE);
        }
      else
        {
          gchar *buf = g_malloc0 (50);
          g_snprintf (buf, 50, "%s, %s", geocode_place_get_name(l->data), geocode_place_get_county (l->data));
          gtk_label_set_text (GTK_LABEL (p->result_labels[i]), buf);
          g_free (buf);

          gtk_widget_set_visible (GTK_WIDGET (p->list_box_rows[i]), TRUE);
          l = l->next;
        }
      ++i;
    }

  //gtk_widget_show_all (p->pop);
  gtk_stack_set_visible_child (GTK_STACK (p->main_stack), p->scrolled_window);

  g_list_free (places_list);
}

GrockSearchPopover *
grock_search_popover_make (GtkSearchEntry *entry)
{
  GrockSearchPopover *p = popover_create (GTK_WIDGET (entry));

  gtk_widget_add_events (p->pop, GDK_KEY_PRESS);

  g_signal_connect_swapped (p->pop, "key_press_event", G_CALLBACK (on_key_press_popover), p->pop);

  gtk_popover_popup (GTK_POPOVER (p->pop));

  return p;
}
